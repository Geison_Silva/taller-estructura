/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_con_menu;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author P13rr00v2
 */
public class Proyecto_con_menu {

    /**
     * @param args the command line arguments
     */
public static void main(String[] args) {
        // TODO code application logic here
        int menu = Integer.parseInt(JOptionPane.showInputDialog(null, "ELIJA EL METODO POR EL CUAL LE GUSTARIA REALIZAR EL ARREGLO"
                + "\n 1. METODO BURBUJA "
                + "\n 2. METODO SHELL"
                + "\n 3. QUICKSORT"
                + "\n 4. SALIR"));

        switch (menu) {
            case 1:
                //METODO BURBUJA
                int arreglo[] = new int[7];
                arreglo[0] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 0 "));
                arreglo[1] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 1 "));
                arreglo[2] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 2 "));
                arreglo[3] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 3 "));
                arreglo[4] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 4 "));
                arreglo[5] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 5 "));
                arreglo[6] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 6 "));

                int auxiliar = 0;
                Scanner in = new Scanner(System.in);

                         
                //Aplicando el metodo de la burbuja
                for (int i = 0; i < arreglo.length - 1; i++) {
                    for (int j = i; j < arreglo.length; j++) {
                        if (arreglo[i] > arreglo[j]) {
                            auxiliar = arreglo[i];
                            arreglo[i] = arreglo[j];
                            arreglo[j] = auxiliar;
                        }
                    }
                }

                //Mostrando los datos ordenados
                for (int i = 0; i < arreglo.length; i++) {
                    JOptionPane.showMessageDialog(null, "LOS NUMEROS ORGANIZADOS POR ESTE METODO  SON: " + arreglo[i] + " ");
                }

                break;

            case 2:
//                METODO SHELL
                int[] A = new int[7];
                A[0] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 0 "));
                A[1] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 1 "));
                A[2] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 2 "));
                A[3] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 3 "));
                A[4] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 4 "));
                A[5] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 5 "));
                A[6] = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese los valores para la posicion 6 "));

                shell(A);
                for (int i = 0; i < 6; i++) {
                    JOptionPane.showMessageDialog(null, "Los numeros ordenados por este metodo son " + A[i]);

                }

                break;
            case 3:
               
                break;
            case 4:
                JOptionPane.showMessageDialog(null, "GRACIAS POR SU VISITA");
                break;
            default:

                JOptionPane.showMessageDialog(null, "OPCION INVALIDA");
        }

    }

// ESTE ES UN COMPONENTE DEL METODO SHELL
    public static void shell(int A[]) {
        int salto, aux, i;
        boolean cambios;
        for (salto = A.length / 2; salto != 0; salto /= 2) {
            cambios = true;
            while (cambios) { // Mientras se intercambie algún elemento
                cambios = false;
                for (i = salto; i < A.length; i++) // se da una pasada
                {
                    if (A[i - salto] > A[i]) { // y si están desordenadospocision en 
                        aux = A[i]; // se reordenan
                        A[i] = A[i - salto];
                        A[i - salto] = aux;
                        cambios = true; // y se marca como cambio.
                    }
                }
            }
        }

    }
}
